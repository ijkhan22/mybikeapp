# How the Project is Done

# Data
1. Data Cleaning and MySQL Table Creation:
   - The first step involved cleaning and preparing the data for storage in a MySQL table.
   - Columns were renamed to follow standard naming conventions.
   - Unwanted columns in the journey table, such as station names, were removed.
   - Stations and journey tables were imported into MySQL.
     - Journey 2015: 814,676 records
     - Journey 2016: 1,863,251 records
     - Journey 2017: 2,911,826 records

2. Data Filtering:
   - Unwanted data was filtered out using the following queries:
     - `DELETE FROM journeys WHERE duration_sec < 10;` (18,623 rows affected)
     - `DELETE FROM journeys WHERE covered_distance_m < 10;` (85,458 rows affected)

3. Journey Data Enrichment:
   - The journey data was appended with additional information from provided Excel files.
   - IDs were added to the journey data for better data management.

4. Indexing for Improved Query Performance:
   - Indexing was implemented to optimize data retrieval speed.
   - Indexes were created on the following columns:
     - `CREATE INDEX idx_id ON stations (id);`
     - `CREATE INDEX idx_id ON journeys (id);`

# APIS
# Stations API

The `station.py` file contains the implementation of the Stations API. This API allows users to perform various operations related to stations.

## StationCollection
- GET `/stations/{page_number}`: Fetches a collection of stations with pagination support. The `page_number` parameter is optional and defaults to 1.
- POST `/stations/{page_number}`: Adds a new station to the collection. The station details should be provided in the request body as JSON.

## StationItem
- GET `/stations/{station_id}`: Retrieves details about a specific station based on the `station_id` provided.
- DELETE `/stations/{station_id}`: Deletes a station with the given `station_id`.

## StationSearch
- GET `/stations/search/{station_name}`: Searches for stations based on the provided `station_name`. Returns a list of matching stations.

## StationTotal
- GET `/stations/total`: Returns the total number of stations in the collection.

The API responses are in the `application/vnd.mason+json` format.

# Journeys API

The `journey.py` file contains the implementation of the Journeys API. This API allows users to perform various operations related to journeys.

## JourneyCollection
- GET `/journeys/{page_number}`: Fetches a collection of journeys with pagination support. The `page_number` parameter is optional and defaults to 1.
- POST `/journeys/{page_number}`: Adds a new journey to the collection. The journey details should be provided in the request body as JSON.

## JourneyItem
- GET `/journeys/{journey_id}`: Retrieves details about a specific journey based on the `journey_id` provided.
- DELETE `/journeys/{journey_id}`: Deletes a journey with the given `journey_id`.

## JourneySearch
- GET `/journeys/search/{journey_name}`: Searches for journeys based on the provided `journey_name`. Returns a list of matching journeys.

## JourneyTotal
- GET `/journeys/total`: Returns the total number of journeys in the collection.

The API responses are in the `application/vnd.mason+json` format.


