# Helsinki city bike app (Dev Academy pre-assignment)
# PROJECT NAME

# About
The Helsinki City Bike App is a web application that provides features to view stations, journeys, and add new stations and journeys. The application follows a client-server architecture with a RESTful API serving as the backend. Data is stored in a MySQL server, and the API endpoints are designed to fetch, search, and insert data from/to the database. Both the backend and frontend components of the application are containerized using Docker.
The application is currently live and can be accessed through the following server: http://86.50.231.48/. Additionally, the API documentation is available at http://86.50.231.48:5000/apidocs, providing detailed information about the available endpoints, request formats, and response structures.

CI/CD pipeline is implemented unit tests. When a user pushes new code to the repository, the code is automatically deployed to a separate server. Subsequently, the tests, including unit tests and lint tests, are executed. If all the tests pass successfully, the project is then deployed to the production server.

# tools and technologies used
Python, Flask, Flask-RESTful, Swagger documentation, HTML, Jinja, AJAX, JavaScript, MySQL, Docker, Linux, SSH, Git, CI/CD, pytest, pylint.

# Functions the project performs
# Project Functions

1. CI/CD Pipeline:
   - A Continuous Integration and Continuous Deployment (CI/CD) pipeline is set up, which automatically runs tests and deploys the application to a Linux server in a Docker container.
   - ![img.png | width=50 ](img.png)
   
2. Unit Tests for APIs:
   - The project includes unit tests for the APIs, ensuring the reliability and functionality of the backend code.
   - Once all tests pass the code is deployed to the production server automatically
   - ![img_1.png | width=50](img_1.png)
   
3. Dockerized Backend:
   - The backend of the project is containerized using Docker, allowing for easy deployment and scalability.
![img_2.png](img_2.png)
4. RESTful APIs for Data Fetching:
   - The project provides RESTful APIs that enable fetching data from the backend. These APIs follow industry-standard practices and conventions.
   - Caching implemented to optimize performance and reduce server load

5. Data Cleaning and Storage:
   - The project ensures that the data is properly cleaned and attributes are appropriately named before being stored in MySQL tables. Indexing is implemented to optimize data retrieval.

6. Stations View with Pagination:
   - The project provides a stations view with pagination support, allowing users to navigate through a large number of stations efficiently.

7. Stations Management:
   - Users have the option to add and delete stations through the application.

8. Journey Statistics:
   - The application displays the total number of journeys starting from a specific station.
   - The application displays the total number of journeys ending at a specific station.

9. Autosuggest/Autocomplete Search:
   - The project includes an autosuggest/autocomplete search feature for stations, providing users with a convenient way to find specific stations.

10. Journey Listing with Double Pagination:
    - Users can view a list of journeys, with support for double pagination for easy navigation through a large number of journeys.

11. Journey Details:
    - Journey details include departure and return stations, covered distance in kilometers, and duration in minutes.

12. Journey Search:
    - Users can search for journeys based on specific criteria, enhancing the overall user experience.

13. Add Journey with Calendar Option:
    - The application allows users to add new journeys, providing a calendar option to select the departure and return time.

14. Stations Name Lookup:
    - Users can search for stations by name, which is particularly useful when adding new journeys.

    
# Project Setup
The project and its database are designed to run in a container, making it easy to set up. To increase modularity, the database is separated as a microservice.
1. Machine Setup
2. Database Setup
3. Application Setup

## 1. Machine Setup
1. Set up an Ubuntu VM or another Linux machine. 
2. Update the system and install the required dependencies by running the following commands:
```
sudo apt-get update
sudo apt-get install vim
sudo apt-get install docker.io
sudo apt-get install docker-compose
```
# 2. Application Setup
Setting up the application follows a simpler process to set up. If you set up the application in a Git repository, it will deploy itself, as the CI/CD pipeline is already configured. You only need to update the IP address in [this](./.gitlab-ci.yml) file. To manually deploy the application, follow these steps:
1. Open the command line on your VM.
2. Clone the repository.
   ```
   git clone https://gitlab.com/ijkhan22/mybikeapp.git
   ```
3. Navigate to the repository folder.
   ```
   cd /mybikeapp
   ```
4. Run the following command for deployment.
   ```
   docker-compose up -d --build
   ```
5. Verify the setup by accessing the documentation link.
   ```
    http://IP_ADDRESS::5000/apidocs/
   ```
6. Log in to the server using the password provided in [this ](https://gitlab.com/ijkhan22/mybikeapp/-/blob/main/bike_api/DataBase/docker-compose.yml)Docker file. Alternatively, you can use another tool like [Navicat](https://www.navicat.com/en/download/navicat-premium ) to access the database.
7. Import the csv files into the database

# 2. Application Setup
Setting up the application follows a similar process to the database setup. If you set up the application in a Git repository, it will deploy itself, as the CI/CD pipeline is already configured. You only need to update the IP address in [this](./.gitlab-ci.yml) file. To manually deploy the application, follow these steps:
1. Open the command line on your VM.
2. Clone the repository.
   ```
   git clone https://gitlab.com/ijkhan22/mybikeapp.git
   ```
3. Navigate to the repository folder.
   ```
   cd /mybikeapp
   ```
4. Run the following command for deployment.
   ```
   docker-compose up -d --build
   ```
5. Verify the setup by accessing this link.
   ```
    http://IP_ADDRESS::5001/
   ```

# 2. Additional Step

This guide will help you set up an SSH key for your Git account and use it to clone repositories using SSH.

1. Generate an SSH key pair

   Run the following command in the terminal, replacing `your_email@example.com` with the email address associated with your Git account:
   ```
   ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
   ```

   When prompted, press Enter to accept the default file location (`~/.ssh/id_rsa`). You can also set a passphrase if you want, but it's optional.

2. Copy the public SSH key

   Display the content of your public SSH key file by running the following command in the terminal:
   ```
     cat ~/.ssh/id_rsa.pub
      ```
   Select and copy the entire key.

3. Add the SSH key to your Git account

   Follow the instructions below to add the public key to your GitHub or GitLab account.


   1. Go to your GitLab account settings: [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys)
   2. Click on "Add an SSH key."
   3. Paste the public key content in the "Key" field, and click "Add key."

4. Set up the SSH agent

   Start the SSH agent in the background and add your private SSH key to the agent:
   ```
   eval "$(ssh-agent -s)"
   ssh-add ~/.ssh/id_rsa
   ```

5. Now you can clone the repository using ssh clone
   ```
   git clone git@gitlab.com:ijkhan22/mybikeapp.git
   ```

Note: If you would like the application to be automatically deployed on your VM, you can configure your VM as a runner for your project. To do this, follow the steps provided in the GitLab settings for your project: https://gitlab.com/ijkhan22/mybikeapp/-/settings/ci_cd