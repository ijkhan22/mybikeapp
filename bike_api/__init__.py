from flask import Flask, jsonify, request, render_template, make_response, Response
from flask_cors import CORS
from flask_caching import Cache

MASON = "application/vnd.mason+json"

SECRET_KEY = 'myverystrongsecretkey'


def create_app():
    """
    Creates a new Flask Application
    Returns :
        A new Flask Instance.
    Raises:
        Nothing
    """
    new_app = Flask(__name__, instance_relative_config=True)
    new_app.config["CACHE_TYPE"] = "FileSystemCache"
    new_app.config["CACHE_DIR"] = "cache"
    new_app.config["SWAGGER"] = {
        "title": "City Bike API",
        "openapi": "3.0.3",
        "uiversion": 3,
        "doc_dir": "./apidocs",
    }
    return new_app


app = create_app()
cache = Cache(app)
CORS(app)
