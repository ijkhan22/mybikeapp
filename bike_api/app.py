from flask import Blueprint
from flask_restful import Api
from flasgger import Swagger

from bike_api import app
from bike_api.resources.station import StationCollection, StationItem,StationSearch,StationTotal
from bike_api.resources.journey import JourneyCollection, JourneyItem,JourneySearch,JourneyTotal


api_bp = Blueprint("api", __name__, url_prefix="/api")  # create a blueprint for apis
api = Api(api_bp)
swagger = Swagger(app, template_file="swagger.yml")
app.register_blueprint(api_bp)  # register the blueprint


api.add_resource(StationCollection, "/stations/", "/stations/<int:page_number>")
api.add_resource(StationItem, "/station/<station_id>")
api.add_resource(StationSearch, "/station/search/<station_name>")
api.add_resource(StationTotal, "/stations/total/")

api.add_resource(JourneyCollection, "/journeys/", "/journeys/<page_number>")
api.add_resource(JourneyItem, "/journey/<journey_id>")
api.add_resource(JourneySearch, "/journey/search/<journey_name>")
api.add_resource(JourneyTotal, "/journeys/total/")

if __name__ == '__main__':
    app.run(debug=True)
