"""this file holds connection and the MySQL quires to access the database"""
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.pool import QueuePool

MYSQL_URL = "mysql+mysqlconnector://{user}:{password}@{host}:{port}/{database}"

db_params = {
    'host': '86.50.231.48',  # local host
    'port': '3306',
    'database': 'bike_app',
    'user': 'myuser',
    'password': 'mypassword'
}
# db_params = {
#     'host': 'localhost',  # local host
#     'port': '3306',
#     'database': 'bike_app',
#     'user': 'root',
#     'password': '1234'
# }

def get_db_conn():
    """
    Opens a new database connection if there is none yet for the current application context
    Returns:
        sqlalchemy.engine.Engine : A database instance for the specified database.

    Raises:
        N/A
    """
    dict_params = dict(db_params)
    target_engine = create_engine(MYSQL_URL.format(**dict_params), poolclass=QueuePool, pool_size=10,
                                  echo=False,
                                  pool_pre_ping=True)
    return target_engine


def query_db(query):
    """
    to do
    Execute a sql query and returns the results

    Args:
        query(str): `sql` variable contains the sql query.

    Returns:
        sqlalchemy.engine.ResultProxy: A SQLAlchemy Result Proxy object representing the query results.

    Raises:
        N/A
    """
    print(query)
    con = get_db_conn()
    query_result = pd.read_sql_query(sql=query, con=con)
    con.dispose()
    return query_result.to_dict(orient='records')


def execute_db(query):
    """
       Executes a sql query and returns the results

       Args:
           query(str) : `sql` variable contains the sql query to execute .

       Returns:
           sqlalchemy.engine.ResultProxy: A SQLAlchemy Result Proxy object representing the query results.

       Raises:
           N/A
       """
    print(query)
    con = get_db_conn()
    query_result = con.execute(query)
    con.dispose()
    return query_result.lastrowid


if __name__ == '__main__':
    SQL = "SELECT * FROM stations where id = 5;"
    station = query_db(SQL)
    print(station)
