import json

from flask import make_response
from flask_restful import Resource
from bike_api import cache
from flask import Response, request
from datetime import datetime
from bike_api.db_util import query_db, execute_db

MASON = "application/vnd.mason+json"


class JourneyCollection(Resource):
    def __init__(self):
        self.journeys = None
        self.sql = None

    @cache.memoize(60)
    def get(self, page_number=1):
        try:
            page_number = (int(page_number) - 1) * 15
            self.sql = f"""SELECT j.*, s.`name` as  departure_station_name, ss.`name` as return_station_name FROM journeys j  
LEFT JOIN stations s on j.departure_station_id = s.id 
LEFT JOIN stations ss on j.return_station_id = ss.id  
ORDER BY j.id DESC limit 15 OFFSET 0"""

            self.journeys = query_db(self.sql)
            journey_json = json.dumps(self.journeys, indent=4)
            response = make_response(journey_json, 200)
            response.headers["Content-Type"] = MASON
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response

    def post(self, page_number=1):
        # Implementation for POST request
        if not request.json:
            return (
                400, "Bad Request",
                "Request body must contain JSON data"
            )

        time_format = '%Y-%m-%d %H:%M:%S'
        time1 = datetime.strptime(request.json['departure_t'], time_format)
        time2 = datetime.strptime(request.json['return_t'], time_format)

        time_difference = time2 - time1
        difference_in_seconds = time_difference.total_seconds()

        if difference_in_seconds < 10 or request.json['covered_distance_m'] < 10:
            print("Issue")
            response = make_response(json.dumps({'Error': 'Distance or time cannot be less then 10'}), 400)
            response.headers["Content-Type"] = MASON
            return response

        request.json['duration_sec'] = difference_in_seconds
        insert_statement = """INSERT INTO journeys (departure_t, `return_t`, departure_station_id, return_station_id, covered_distance_m, duration_sec)
VALUES ('{departure_t}', '{return_t}', {departure_station_id}, {return_station_id}, {covered_distance_m}, {duration_sec});
"""
        insert_statement = insert_statement.format(**request.json)
        last_row_id = execute_db(insert_statement)
        response = make_response(json.dumps({'journey_id': last_row_id}), 201)
        response.headers["Content-Type"] = MASON
        return response


class JourneyItem(Resource):
    def __init__(self):
        self.sql = None

    @cache.memoize(60)
    def get(self, journey_id):
        try:
            self.sql = f"""SELECT * FROM journeys where id = {journey_id};
            """

            self.journeys = query_db(self.sql)[0]
            journey_json = json.dumps(self.journeys, indent=4)
            response = make_response(journey_json, 200)
            response.headers["Content-Type"] = MASON
            print(f"journeys response {response.json}")
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response

    def delete(self, journey_id):
        try:
            self.sql = f"DELETE FROM journeys WHERE id = {journey_id};"
            print(self.sql)
            execute_db(self.sql)
            return Response(status=204)
        except Exception as e:
            error_msg = json.dumps(e)
            response = make_response(error_msg, 400)
            response.headers["Content-Type"] = MASON
            return response


class JourneySearch(Resource):
    def __init__(self):
        self.journeys = None
        self.sql = None

    @cache.memoize(60)
    def get(self, journey_name):
        try:
            self.sql = f"""SELECT s.`name`, j.id FROM journeys j JOIN stations s on j.departure_station_id = s.id 
            where s.`name` like '%{journey_name}%' LIMIT 10;"""
            print(self.sql)
            self.journeys = query_db(self.sql)
            journey_json = json.dumps(self.journeys, indent=4)
            response = make_response(journey_json, 200)
            response.headers["Content-Type"] = MASON
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response


class JourneyTotal(Resource):
    def __init__(self):
        self.journeys = None
        self.sql = None

    @cache.memoize(60)
    def get(self):
        try:
            self.sql = f"SELECT COUNT(1) as total_journeys from journeys;"

            self.journeys = query_db(self.sql)[0]
            journey_json = json.dumps(self.journeys, indent=4)
            response = make_response(journey_json, 200)
            response.headers["Content-Type"] = MASON
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response
