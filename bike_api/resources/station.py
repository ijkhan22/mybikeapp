import json

from flask import make_response
from flask_restful import Resource
from bike_api import cache
from flask import Response, request

from bike_api.db_util import query_db, execute_db

MASON = "application/vnd.mason+json"


class StationCollection(Resource):
    def __init__(self):
        self.stations = None
        self.sql = None

    @cache.memoize(60)
    def get(self, page_number=1):
        try:
            page_number = (int(page_number) - 1) * 15
            self.sql = f"SELECT * FROM stations ORDER BY id DESC limit 15 OFFSET {page_number};"

            self.stations = query_db(self.sql)
            station_json = json.dumps(self.stations, indent=4)
            response = make_response(station_json, 200)
            response.headers["Content-Type"] = MASON
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response

    def post(self, page_number=1):
        # Implementation for POST request
        if not request.json:
            return (
                400, "Bad Request",
                "Request body must contain JSON data"
            )
        insert_statement = """INSERT INTO stations (nimi, namn, name, osoite, adress, kaupunki, stad, operaattor, kapasiteet, x, y)
                            VALUES ('{nimi}', '{namn}', '{name}', '{osoite}', '{adress}','{kaupunki}',' {stad}','{operaattor}','{kapasiteet}','{x}','{y}');"""
        insert_statement = insert_statement.format(**request.json)
        last_row_id = execute_db(insert_statement)
        response = make_response(json.dumps({'station_id': last_row_id}), 201)
        response.headers["Content-Type"] = MASON
        return response


class StationItem(Resource):
    def __init__(self):
        self.sql = None

    @cache.memoize(60)
    def get(self, station_id):
        try:
            self.sql = f"""SELECT
                                s.*,
                                IFNULL(dj.total_departure_journeys, 0) AS total_departure_journeys,
                                IFNULL(rj.total_return_journeys, 0) AS total_return_journeys,
                                IFNULL(dj.avg_departure_distance, 0) AS avg_departure_distance_m,
                                IFNULL(rj.avg_return_distance, 0) AS avg_return_distance_m
                            FROM
                                stations s
                            LEFT JOIN (
                                SELECT
                                    s.ID,
                                    COUNT(1) AS total_departure_journeys,
                                    AVG(j.covered_distance_m) AS avg_departure_distance
                                FROM
                                    stations s
                                JOIN journeys j ON s.ID = j.departure_station_id AND s.ID = {station_id}
                                GROUP BY
                                    s.ID,
                                    s.`Name`
                            ) dj ON s.ID = dj.ID
                            LEFT JOIN (
                                SELECT
                                    s.ID,
                                    COUNT(1) AS total_return_journeys,
                                    AVG(j.covered_distance_m) AS avg_return_distance
                                FROM
                                    stations s
                                JOIN journeys j ON s.ID = j.return_station_id AND s.ID = {station_id}
                                GROUP BY
                                    s.ID,
                                    s.`Name`
                            ) rj ON s.ID = rj.ID
                            WHERE
                                s.ID = {station_id};;
                                        """

            self.stations = query_db(self.sql)[0]
            station_json = json.dumps(self.stations, indent=4)
            response = make_response(station_json, 200)
            response.headers["Content-Type"] = MASON
            print(f"stations response {response.json}")
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response

    def delete(self, station_id):
        try:
            self.sql = f"DELETE FROM stations WHERE id = {station_id};"
            execute_db(self.sql)
            return Response(status=204)
        except Exception as e:
            error_msg = json.dumps(e)
            response = make_response(error_msg, 400)
            response.headers["Content-Type"] = MASON
            return response


class StationSearch(Resource):
    def __init__(self):
        self.stations = None
        self.sql = None

    @cache.memoize(60)
    def get(self, station_name):
        try:
            self.sql = f"SELECT `name`,id FROM stations where `name` like '%{station_name}%' LIMIT 10;"

            self.stations = query_db(self.sql)
            station_json = json.dumps(self.stations, indent=4)
            response = make_response(station_json, 200)
            response.headers["Content-Type"] = MASON
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response


class StationTotal(Resource):
    def __init__(self):
        self.stations = None
        self.sql = None

    @cache.memoize(60)
    def get(self):
        try:
            self.sql = f"SELECT COUNT(1) as total_stations from stations;"

            self.stations = query_db(self.sql)[0]
            station_json = json.dumps(self.stations, indent=4)
            response = make_response(station_json, 200)
            response.headers["Content-Type"] = MASON
            return response

        except Exception as e:
            error = {"error": str(e)}
            error_json = json.dumps(error, indent=4)
            response = make_response(error_json, 500)
            response.headers["Content-Type"] = MASON
            return response
