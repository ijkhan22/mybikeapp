import math

from flask import Blueprint, jsonify

from flask import flash, render_template, request, redirect, url_for
import requests
from flask_paginate import Pagination, get_page_parameter
from client_app import API_BASE_URL

journey_bp = Blueprint('journey', __name__)

page_data = {'journeys': None}


@journey_bp.route('/journeys/')
def home():
    page = request.args.get('page', 1, type=int)
    print(f"Requesting page {page} on journeys home page")
    response = requests.get(f'{API_BASE_URL}journeys/{page}')
    journeys = response.json()
    page_data['journeys'] = journeys

    # get total journey
    response = requests.get(f'{API_BASE_URL}journeys/total')
    total_journeys = response.json()['total_journeys']
    print(f"Total journey are {total_journeys}")
    per_page = 15
    total_pages = math.ceil(total_journeys / per_page)  # Calculate the total pages

    page_data['start_page'] = 1 if ((page // 20) * 20) == 0 else ((page // 20) * 20)
    page_data['end_page'] = page_data['start_page'] + 20

    pagination = Pagination(page=page, total=total_journeys, per_page=per_page, bs_version=4)
    return render_template("journeys.html", page_data=page_data, pagination=pagination, total_pages=total_pages)


@journey_bp.route('/journey/<int:journey_id>')
def journey_details(journey_id):
    page_data = {}
    print(f"getting details of journey {journey_id}")
    response = requests.get(f'{API_BASE_URL}journey/{journey_id}')
    journey = response.json()
    page_data['journey'] = journey
    return render_template('journey_details.html', page_data=page_data)


@journey_bp.route('/journey/add', methods=['POST', 'GET'])
def add_journey():
    """
     Adds journey  information from API endpoint and renders it on add_journey.html
     """
    if request.method == 'GET':
        # get list of journey so that user can select from them
        # Get the existing journey from the API for keys to make form dynamic
        journey = requests.get(f'{API_BASE_URL}journey/{20}', timeout=20).json()
        print("Rendering Add form for journeys")
        return render_template('add_journey.html', journey=journey, page_data=page_data)

    elif request.method == 'POST':
        print(f"inserting new journey {request.form}")
        journey = {}
        user_inputs = ''
        try:
            for key, user_inputs in request.form.items():
                if key == 'covered_distance_m':
                    journey[key] = int(user_inputs)
                elif key == 'departure_station_id':
                    journey[key] = int(user_inputs)
                elif key == 'return_station_id':
                    journey[key] = int(user_inputs)
                else:
                    journey[key] = user_inputs
        except ValueError:
            flash(f'Enter Valid Numbers {user_inputs}', 'danger')
            return redirect(url_for('journey.add_journey'))
        # Send a PUT request to update the journey in the API
        response = requests.post(f'{API_BASE_URL}journeys/1', json=journey, timeout=20)
        if response.status_code == 201:
            print("journey added successfully")
            # if the edit is successful go to the journey details page
            flash('journey added successfully', 'success')
            return redirect(url_for('journey.home'))
        # Error updating journey
        flash(f'Error! adding journey. {response.status_code}|{response.text}', 'danger')
        return redirect(url_for('journey.add_journey'))
        return redirect(url_for('journey.add_journey'))


@journey_bp.route('/journey/getjourneylist', methods=['GET'])
def get_journey_list():
    search_query = request.args.get('search', '')
    print(f"Searching for {search_query}")
    # Perform the necessary logic to retrieve journey suggestions based on the search query

    # Assuming you have a list of journey objects
    response = requests.get(f'{API_BASE_URL}journey/search/{search_query}')
    journeys = response.json()
    print(journeys)

    # Filter the journeys based on the search query
    filtered_journeys = [journey for journey in journeys if search_query.lower() in journey["name"].lower()]

    # Return the filtered journeys as a JSON response
    return jsonify(filtered_journeys)
