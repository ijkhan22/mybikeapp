import math

from flask import Blueprint, jsonify

from flask import flash, render_template, request, redirect, url_for
import requests
from flask_paginate import Pagination, get_page_parameter
from client_app import API_BASE_URL

station_bp = Blueprint('station', __name__)

page_data = {'stations': None}



@station_bp.route('/')
def home():
    page = request.args.get('page', 1, type=int)
    print(f"Requesting page {page} on stations home page")
    response = requests.get(f'{API_BASE_URL}stations/{page}')
    stations = response.json()
    page_data['stations'] = stations

    #get total stations
    response = requests.get(f'{API_BASE_URL}stations/total')
    total_stations = response.json()['total_stations']
    per_page = 15
    total_pages = math.ceil(total_stations / per_page)  # Calculate the total pages
    pagination = Pagination(page=page, total=total_stations, per_page=per_page, bs_version=4)
    return render_template("stations.html", page_data=page_data, pagination=pagination, total_pages=total_pages)


@station_bp.route('/station/<int:station_id>')
def station_details(station_id):
    page_data = {}
    print(f"getting details of station {station_id}")
    response = requests.get(f'{API_BASE_URL}station/{station_id}')
    station = response.json()
    page_data['station'] = station
    return render_template('station_details.html', page_data=page_data)


@station_bp.route('/station/add', methods=['POST', 'GET'])
def add_station():
    """
     Adds station  information from API endpoint and renders it on add_station.html
     """
    if request.method == 'GET':
        # Get the existing station from the API for keys to make form dynamic
        station = requests.get(f'{API_BASE_URL}station/{20}', timeout=20).json()
        print("Rendering Add form for stations")
        return render_template('add_station.html', station=station, page_data=page_data)

    elif request.method == 'POST':

        station = {}
        user_inputs = ''
        # looping through form to make it dynamic
        try:
            for key, user_inputs in request.form.items():
                if key == 'kapasiteet':
                    station[key] = int(user_inputs)
                elif key == 'x':
                    station[key] = float(user_inputs)
                elif key == 'y':
                    station[key] = float(user_inputs)
                else:
                    station[key] = user_inputs
        except ValueError:
            flash(f'Enter Valid Numbers {user_inputs}', 'danger')
            return redirect(url_for('station.add_station'))
        # Send a PUT request to update the station in the API
        response = requests.post(f'{API_BASE_URL}stations/1', json=station, timeout=20)
        if response.status_code == 201:
            print("station added successfully")
            # if the edit is successful go to the station details page
            flash('station added successfully', 'success')
            return redirect(url_for('station.home'))
        # Error updating station
        flash(f'Error! adding station. {response.status_code}', 'danger')
        return redirect(url_for('station.add_station'))

@station_bp.route('/station/<int:station_id>/delete', methods=['POST'])
def delete_station(station_id):
    """
     Deletes station record from API endpoint and navigates to home.html to render stations records on home.html

     """
    response = requests.delete(f'{API_BASE_URL}station/{station_id}')
    if response.status_code == 204:
        return redirect(url_for('station.home'))
    return f'Error: {response.status_code}', response.status_code


@station_bp.route('/station/getstationlist', methods=['GET'])
def get_station_list():
    search_query = request.args.get('search', '')
    print(f"Searching for {search_query}")
    # Perform the necessary logic to retrieve station suggestions based on the search query

    # Assuming you have a list of station objects
    response = requests.get(f'{API_BASE_URL}station/search/{search_query}')
    stations = response.json()
    print(stations)

    # Filter the stations based on the search query
    filtered_stations = [station for station in stations if search_query.lower() in station["name"].lower()]

    # Return the filtered stations as a JSON response
    return jsonify(filtered_stations)