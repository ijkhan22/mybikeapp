"""This file will start the web application, for the application to run the apis should also be running"""

from client_app.routes.stations import station_bp
from client_app.routes.journeys import journey_bp

from client_app import app

app.register_blueprint(station_bp)
app.register_blueprint(journey_bp)

if __name__ == '__main__':
    app.run(debug=True, port=80)
