  const searchForm = document.getElementById("searchForm");
  const searchInput = document.getElementById("searchInput");
  const suggestionBox = document.getElementById("suggestionBox");

  searchInput.addEventListener("input", function(event) {
    const searchQuery = event.target.value;
    if (searchQuery.length > 3) {
      fetch(`/journey/getjourneylist?search=${encodeURIComponent(searchQuery)}`)
        .then((response) => response.json())
        .then((data) => {
          suggestionBox.innerHTML = "";
          data.forEach((journey) => {
            const suggestion = document.createElement("p");
            const link = document.createElement("a");
            link.textContent = journey.name;
             link.href = `/journey/${journey.id}`;
            link.target = "_blank";
            suggestion.appendChild(link);
            suggestionBox.appendChild(suggestion);
          });
        });
    } else {
      suggestionBox.innerHTML = ""; // Clear the suggestion box if search query length is not sufficient
    }
  });