// Wait for the DOM to be fully loaded
document.addEventListener("DOMContentLoaded", function () {
  const searchForm = document.getElementById("searchForm");
  const searchInput = document.getElementById("searchInput");
  const suggestionBox = document.getElementById("suggestionBox");

  searchInput.addEventListener("input", function (event) {
    const searchQuery = event.target.value;
    if (searchQuery.length > 3) {
      fetch(`/station/getstationlist?search=${encodeURIComponent(searchQuery)}`)
        .then((response) => response.json())
        .then((data) => {
          suggestionBox.innerHTML = "";
          data.forEach((station) => {
            const suggestion = document.createElement("p");
            const link = document.createElement("a");
            link.textContent = station.name;
            link.href = `/station/${station.id}`;
            link.target = "_blank";
            suggestion.appendChild(link);
            suggestionBox.appendChild(suggestion);
          });
        });
    } else {
      suggestionBox.innerHTML = ""; // Clear the suggestion box if search query length is not sufficient
    }
  });
});
