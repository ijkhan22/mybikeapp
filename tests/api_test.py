"""
This file tests all the apis of bike api, the file takes a test_data_obj and inserts it into the db,
then quires back and validates the object, finally it tests the delete request by deleting the test_data_obj
"""
from bike_api.app import app

STATION_TEST_DATA = {'nimi': 'TEST', 'namn': 'TEST', 'name': 'TEST',
                     'osoite': 'TEST 1', 'adress': 'TEST ADD 1', 'kaupunki': None, 'stad': None,
                     'operaattor': None, 'kapasiteet': 30, 'x': 24.9502, 'y': 60.1554}

JOURNEY_TEST_DATA = {
    "departure_t": "2023-05-18 09:00:00",
    "return_t": "2023-05-18 15:00:00",
    "departure_station_id": 1,
    "return_station_id": 2,
    "covered_distance_m": 1000
}


def test_get_stations():
    """
    Test function for the /api/stations/ endpoint.
    :return: pass or fail
    """

    response = app.test_client().get('/api/stations/1')
    assert response.status_code == 200


def test_insert_delete_stations():
    """
    method to test the insert and delete of stations, we will use STATION_TEST_DATA variable
    :return: pass or fail
    """
    print(STATION_TEST_DATA)
    response = app.test_client().post('/api/stations/1', json=STATION_TEST_DATA)
    assert response.status_code == 201

    station_id = response.json["station_id"]
    delete_station(station_id)


def delete_station(station_id):
    print(f"Deleting station {station_id}")
    response = app.test_client().delete(f'/api/station/{station_id}')
    assert response.status_code == 204


# for journey

def test_get_journeys():
    """
    Test function for the /api/journeys/ endpoint.
    :return: pass or fail
    """

    response = app.test_client().get('/api/journeys/1')
    assert response.status_code == 200


def test_insert_delete_journeys():
    """
    method to test the insert and delete of journeys, we will use journey_TEST_DATA variable
    :return: pass or fail
    """
    print(JOURNEY_TEST_DATA)
    response = app.test_client().post('/api/journeys/1', json=JOURNEY_TEST_DATA)
    assert response.status_code == 201

    journey_id = response.json["journey_id"]
    delete_journey(journey_id)


def delete_journey(journey_id):
    print(f"Deleting journey {journey_id}")
    response = app.test_client().delete(f'/api/journey/{journey_id}')
    assert response.status_code == 204
